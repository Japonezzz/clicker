﻿using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class PanelsBehavior : MonoBehaviour
{
    #region Inspector variables

    [SerializeField] private GameObject _startPanel;
    [SerializeField] private GameObject _countDownPanel;
    [SerializeField] private GameObject _gamePanel;
    [SerializeField] private GameObject _finalPanel;
    [SerializeField] private GameObject _appearIcon;

    [SerializeField] private Text _countDownText;
    [SerializeField] private Text timerText;

    #endregion

    #region Member variables

    private Stopwatch stopwatch;

    private GameObject icon;
    private GameObject[] panels;

    #endregion


    #region Unity methods
    private void Awake()
    {
        panels = new GameObject[4];
        panels[0] = _startPanel;
        panels[1] = _countDownPanel;
        panels[2] = _gamePanel;
        panels[3] = _finalPanel;
    }
    void Start()
    {
        ChangePanel(0);
    }

    #endregion


    #region Private methods
    private void ChangePanel( int panelNumber)
    {
        for (int i = 0; i < panels.Length; i++)
        {
            if (i != panelNumber)
            {
                panels[i].SetActive(false);
            }
            else
            {
                panels[i].SetActive(true);
            }
        }
    }

    

    private IEnumerator StartCountDown()
    {
        for (int i = 5; i >= 0; i--)
        {
            if (i == 0)
            {
                ChangePanel(2);
                Instantiate_icon();
            }

            _countDownText.text = i.ToString();
            yield return new WaitForSeconds(1);
           
        }
    }


    private void Instantiate_icon()
    {
        var width = panels[2].GetComponent<RectTransform>().rect.width;
        var height = panels[2].GetComponent<RectTransform>().rect.height;

        var iconsize = Mathf.Min(Screen.width, Screen.height)/10;

        icon = Instantiate(_appearIcon, panels[2].transform);

        var x = Random.Range(-width / 2 + icon.GetComponent<RectTransform>().rect.width/2, width / 2 - icon.GetComponent<RectTransform>().rect.width / 2);
        var y = Random.Range(-height / 2 + icon.GetComponent<RectTransform>().rect.height / 2, height / 2 - icon.GetComponent<RectTransform>().rect.height / 2);

        icon.GetComponent<RectTransform>().localPosition = new Vector2(x,y);
        icon.GetComponent<Button>().onClick.AddListener(IconClick);

        var iconTransform = icon.transform as RectTransform;
        iconTransform.sizeDelta = new Vector2(iconsize, iconsize);

        stopwatch = new Stopwatch();
        stopwatch.Start();

    }

    private void IconClick()
    {
        Destroy(icon);

        stopwatch.Stop();
        
        TimeSpan ts = stopwatch.Elapsed;

        string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);

        ChangePanel(3);

        timerText.text = elapsedTime;
    }

    #endregion


    #region Public methods
    public void StartBtnClc()
    {
        ChangePanel(1);
        StartCoroutine(StartCountDown());
    }


    public void Restart_game()
    {
        ChangePanel(1);
        StartCoroutine(StartCountDown());
    }

    #endregion
}